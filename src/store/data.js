import api from "../api"
import config from "../config.json"
import Vue from "vue";

export default {

    namespaced: true,

    state: {
        messages: [],
        templates: config.templates,
        sending: false
    },

    mutations: {
        addMessage(state, {jid, template, to}) {
            state.messages.push({
                jid,
                template,
                to,
                status: config.statuses.queued
            })
        },
        setMessageStatus(state, {jid, status}) {
            let index = state.messages.findIndex(message => message.jid === jid)
            if (index >= -1) {
                Vue.set(state.messages[index], 'status', status)
            }
        },

        setSending(state, value) {
            state.sending = value
        }
    },

    actions: {
        sendMessage(state, data) {
            state.commit('setSending', true)
            let promise = api.sendMessage(data)
            promise.then((response) => {
                let {jid} = response.data
                let {template, to} = data
                state.commit('addMessage', {jid, template, to})
            }).catch(() => {

            }).finally(() => {
                state.commit('setSending', false)
            })
            return promise
        },

        updateStatus(state, jid) {
            let promise = api.checkStatus(jid)
            promise.then((response) => {
                let {status} = response.data
                state.commit('setMessageStatus', {jid, status})
            }).catch(() => {

            }).finally(() => {

            })
            return promise
        }
    }
}