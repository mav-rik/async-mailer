export default {

    app: {
        title: "Async Mailer",
        messages: "Messages",
        template: "Template",
        send: "Send",
        email: "E-mail",
        invalidEmail: "Invalid e-mail.",
        required: "Required."
    }

}