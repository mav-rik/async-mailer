/**
 * Repeats repeatedFn until returnFn returns true each intervalTime ms
 *
 * @param {Function} repeatedFn     -   Function that will be repeated. Promise is expected but supposed to work anyways
 * @param {Function} returnCheckFn  -   Function that checks the condition of stop repeating
 * @param {Number} intervalTime     -   Interval time
 * @param {Boolean} immediately     -   runs repeatedFn right the way before the very first interval
 */
const asyncLoop = (repeatedFn, returnCheckFn, intervalTime, immediately) => {
    let i = setInterval(loopFn, intervalTime)
    if (immediately) {
        loopFn()
    }
    function loopFn () {
        let result = repeatedFn()
        if (result && typeof result.then === 'function' && typeof result.catch === 'function') {
            result.then((r)=>{
                checkReturn(r)
            }).catch(()=>{clearInterval(i)})
        } else {
            checkReturn(result)
        }
    }
    function checkReturn(result) {
        if (returnCheckFn(result)) {
            clearInterval(i)
        }
    }
}

export default asyncLoop