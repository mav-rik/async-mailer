import axios from "axios"

class MailerApi {
    constructor() {
        this.axios = axios.create({
            baseURL: '/message/'
        })
    }

    sendMessage({template, to}) {
        return this.axios.post('send', {
            template,
            to
        })
    }

    checkStatus(jid) {
        return this.axios.get([jid, 'status'].join('/'))
    }
}

export default MailerApi