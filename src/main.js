import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.css'
import store from './store'
import en from './locales/en'

Vue.use(Vuetify, {
    lang: {
        locales: { en },
        current: 'en'
    }
})

new Vue({
    el: '#app',
    store,
    render: h => h(App)
})
